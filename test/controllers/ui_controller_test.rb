require 'test_helper'

class UiControllerTest < ActionDispatch::IntegrationTest
  test "should get docs" do
    get ui_docs_url
    assert_response :success
  end

  test "should get home" do
    get ui_home_url
    assert_response :success
  end

end
