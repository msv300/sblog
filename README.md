# FrescoDocs

API for Messaging application

## System Dependencies

  * [memcached](http://memcached.org/)
  * [PostgreSQL](http://www.postgresql.org/)
  * [Redis](http://redis.io/)
  * [ImageMagick](http://www.imagemagick.org/script/index.php)

## Clone the repository

```
git clone https://code.labs.futureworkplace.in/fresco/docs.git
```

## Install Gems
```
bundle install --local
```

## Copy sample files and edit them as appropriate
```
cp config/database.yml.sample config/database.yml
cp config/dalli.yml.sample config/dalli.yml
cp config/paperclip.yml.sample config/paperclip.yml
cp config/secrets.yml.sample config/secrets.yml
cp config/app_settings.yml.sample config/app_settings.yml
```

## Database initialization

```
bundle exec rake db:setup
bundle exec rake activities:db:create
bundle exec rake activities:db:migrate

```
bundle exec rake swagger:docs
```
## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request