class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
      t.attachment :image
      t.integer :pic_type
      t.references :parent, polymorphic: true

      t.timestamps
    end
  end
end
