# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(  email: 'admin@lblog.com',
              password: 'admin#123',
              password_confirmation: 'admin#123',
              user_name: 'admin',
              first_name: 'Admin',
              last_name: 'lblog',
              status: 'active',
              activated_at: '2016-11-24 10:32:07')

admin = User.where(email: 'admin@lblog.com').first
admin.add_role :admin

# Creating Default profile and cover pictures

profile_image_path = "#{Rails.root}/app/assets/images/default_profile.png"
profile_image_file = File.new(profile_image_path)

Picture.create( pic_type: 0,
                image: ActionDispatch::Http::UploadedFile.new(
                        :filename => File.basename(profile_image_file),
                        :tempfile => profile_image_file,
                        :type => MIME::Types.type_for(profile_image_path).first.content_type ))

cover_image_path = "#{Rails.root}/app/assets/images/default_cover.png"
cover_image_file = File.new(cover_image_path)

Picture.create( pic_type: 1,
                image: ActionDispatch::Http::UploadedFile.new(
                        :filename => File.basename(cover_image_file),
                        :tempfile => cover_image_file,
                        :type => MIME::Types.type_for(cover_image_path).first.content_type ))