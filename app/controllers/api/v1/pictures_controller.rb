class Api::V1::PicturesController < ApiBaseController
  load_and_authorize_resource

  swagger_controller :pictures, "Pictures Management"

  swagger_api :index do
    summary 'Lists all Pictures that user has uploaded'
    param_list :query, :parent_type, :string, :optional, 'Image for a User or a Post', Picture::ParentType::ALL
    param :query, :limit, :integer, :optional, 'Number of Posts per page'
    param :query, :page_number, :integer, :optional, 'Page Number'
    response :ok
    response :unauthorized
    response :forbidden
  end

  def index
    @pictures = current_user.get_pictures(params[:parent_type])
    limit = Post::RECENT_RECORDS_LIMIT
    if params[:limit].present?
      limit = params[:limit].to_i > Post::RECENT_RECORDS_LIMIT ? Post::RECENT_RECORDS_LIMIT : params[:limit].to_i
    end
    @pictures = @pictures.page(params[:page_number]).per(limit)
  end

  swagger_api :create do
    summary 'Create new Picture'
    param_list :form, :parent_type, :string, :required, 'Parent type should be User or Post', Picture::ParentType::ALL
    param :form, :parent_id, :integer, :required, 'ID of the Parent i.e., the User or Post'
    param :form, :'picture[image]', :file, :required, 'Image attachment'
    param_list :form, :'picture[pic_type]', :string, :optional, 'Picture type Profile or Cover only if the parent_type is User', Picture::PicType::ALL
    response :created
    response :bad_request
    response :forbidden
    response :unauthorized
  end

  def create
    if Picture::ParentType::ALL.include?(params[:parent_type]) && params[:parent_id].present?
      if params[:picture][:pic_type].present? && Picture::PicType::ALL.exclude?(params[:picture][:pic_type])
        raise App::Exception::InvalidParameter.new(_('errors.pictures.picture_type_not_applicable'))
      end
      @user_or_post = params[:parent_type].constantize.find(params[:parent_id])
      @picture = @user_or_post.pictures.new(picture_params)
      if @picture.save
        render 'show', status: :created
      else
        render 'shared/model_errors', locals: { object: @picture }, status: :bad_request
      end
    else
      raise App::Exception::InvalidParameter.new(_('errors.pictures.missing_parent_type'))
    end
  end

  swagger_api :destroy do
    summary "Destroy a Picture"
    param :path, :id, :integer, :required, 'Picture ID'
    response :ok
    response :bad_request
    response :forbidden
    response :unauthorized
  end

  def destroy
    @picture.delete
    head :ok
  end

  swagger_api :show do
    summary 'Display a Pictures details'
    param :path, :id, :integer, :required, 'Picture ID'
    response :ok
    response :bad_request
    response :forbidden
    response :unauthorized
  end

  def show
  end

  private
    def picture_params
      params.require(:picture).permit(:image, :pic_type)
    end
end
