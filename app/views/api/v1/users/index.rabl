object false

node :users do
  partial 'users/show', object: @users
end

node(:_links) do
  paginate @users
end