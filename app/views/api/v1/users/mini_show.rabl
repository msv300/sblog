object @user

attributes :id, :email, :user_name, :first_name, :last_name

node( :profile_picture, if: lambda{ |user| user.profile_picture.present? } ) do |user|
  picture_url(user.profile_picture, :small)
end