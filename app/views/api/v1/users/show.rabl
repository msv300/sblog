object @user

attributes :id, :email, :user_name, :first_name, :last_name, :type, :status, :created_at, :updated_at

node false do |user|
  partial('pictures/image_style', object: user.profile_picture,
    locals: { pic_type: Picture::PicType::PROFILE, node_name: "profile_picture" })
end

node :cover_picture do |user|
  picture_url(user.cover_picture, :original)
end