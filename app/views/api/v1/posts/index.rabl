object false

node :posts do
  partial 'posts/show', object: @posts
end

node :users do
  partial 'users/list', object: load_associated_users(@posts)
end

node(:_links) do
  paginate @posts
end