object @post

attributes :id, :body, :user_id, :created_at, :updated_at

node( :pictures, if: lambda{ |post| post.pictures.present? } ) do |post|
  partial('pictures/list', object: post.pictures)
end