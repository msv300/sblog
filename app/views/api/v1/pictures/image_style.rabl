object @picture

node_name = locals[:node_name].present? ? locals[:node_name] : "image"

node node_name.to_sym do |picture|
  {
    small_url: picture_url(picture, :small),
    medium_url: picture_url(picture, :medium),
    original_url: picture_url(picture, :original)
  }
end