object false

node :pictures do
  partial 'pictures/show', object: @pictures
end

node(:_links) do
  paginate @pictures
end
