object @picture

attributes :id, :created_at, :updated_at

node :name do |picture|
  picture.image_file_name
end

node :content_type do |picture|
  picture.image_content_type
end

node :size do |picture|
  picture.image_file_size
end

node :pic_type do |picture|
  picture.pic_type
end

node :owner_id do |picture|
  picture.owner.id
end

extends 'pictures/image_style'