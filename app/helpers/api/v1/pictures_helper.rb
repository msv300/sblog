module Api::V1::PicturesHelper

  def picture_url(picture, style = :original)
    if picture.present?
      picture.image.url(style)
    end
  end

end
