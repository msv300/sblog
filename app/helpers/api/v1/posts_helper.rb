module Api::V1::PostsHelper

  def load_associated_users(post_to_posts)
    User.where(id: post_to_posts.pluck(:user_id).uniq)
  end

end
