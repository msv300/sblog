class Post < ApplicationRecord
  belongs_to :user, inverse_of: :posts
  has_many :pictures, as: :parent

  validates :body, presence: true, length: { maximum: 1000 }

  before_destroy :delete_associated_pictures

  default_scope { order(created_at: :desc) }

  RECENT_RECORDS_LIMIT = 50

  def delete_associated_pictures
    self.pictures.destroy_all
  end

end
