class Picture < ApplicationRecord
  belongs_to :parent, polymorphic: true

  module PicType
    PROFILE = 'profile'
    COVER = 'cover'
    ALL = Picture::PicType.constants.map{ |status| Picture::PicType.const_get(status) }.flatten.uniq
  end

  module ParentType
    USER = 'User'
    POST = 'Post'
    ALL = Picture::ParentType.constants.map{ |status| Picture::ParentType.const_get(status) }.flatten.uniq
  end

  has_attached_file :image,
    styles: { small: ['45x45#', :jpg], medium: ['120x120#', :jpg], large: ['200x200', :jpg] },
    convert_options: { small: "-quality 75 -strip",
                       medium: "-quality 85 -strip",
                       large: "-quality 100 -strip" }

  validates_attachment :image, 
    presence: true,
    content_type: { content_type: /\Aimage\/.*\Z/ },
    size: { in: 0..AppSettings[:picture][:max_allowed_size].megabytes }

  validates :pic_type, inclusion: { in: PicType::ALL, allow_nil: true }
  validate :cannot_set_pic_type_for_post,
    if: Proc.new { |picture| picture.pic_type.present? && Picture::PicType::ALL.include?(picture.pic_type) && picture.parent_type == ParentType::POST }

  before_save :unset_current_profile_or_cover,
    if: Proc.new { |picture| picture.pic_type.present? && Picture::PicType::ALL.include?(picture.pic_type) && picture.parent_type == ParentType::USER }

  enum pic_type: [:profile, :cover]

  default_scope { order(created_at: :desc) }

  def owner
    self.parent_type == ParentType::USER ? self.parent : self.parent.user
  end

  def unset_current_profile_or_cover
    profile_or_cover = self.parent.pictures.send(self.pic_type).first
    if profile_or_cover.present? 
      profile_or_cover.update(pic_type: nil)
    end
  end

  def cannot_set_pic_type_for_post
    self.errors[:messages] << _('errors.pictures.cannot_set_pic_type_for_post')
  end

  def self.default_profile
    where(parent: nil, pic_type: Picture.pic_types[:profile]).first
  end

  def self.default_cover
    where(parent: nil, pic_type: Picture.pic_types[:cover]).first
  end
end
