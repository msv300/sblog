module App
  module Exception
    class InvalidParameter < ArgumentError; end
  end
end