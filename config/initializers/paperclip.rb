config = Rails.application.config_for(:paperclip).symbolize_keys!

Paperclip::Attachment.default_options.merge!(config)