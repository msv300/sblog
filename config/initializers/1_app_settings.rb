AppSettings = Rails.application.config_for(:app_settings).deep_symbolize_keys!

def build_url()
  if ![443, 80].include?(AppSettings[:port].to_i)
    custom_port = ":#{AppSettings[:port]}"
  else
    custom_port = nil
  end
  app_path =
  [ AppSettings[:protocol],
    "://",
    AppSettings[:host],
    custom_port,
    AppSettings[:relative_url_root],
  ].join('')
end

AppSettings[:api_version] ||= 'v1'
AppSettings[:host] ||= 'localhost'
AppSettings[:relative_url_root] ||= ''
AppSettings[:url] = build_url