Rails.application.routes.draw do
  
  namespace :api do
    namespace :v1 do

      resources :users do
        collection do
          get 'profile/:username', to: 'users#details'
          get :profile
        end
        member do
          put :block
          put :activate
        end
      end

      resources :posts

      resources :pictures

      resources :roles do
        collection do
          post :create
          delete :destroy
          get :users_list
        end
      end

    end
  end
  
  post   'login'   =>  'sessions#create'
  delete 'logout'  =>  'sessions#destroy'

  root 'ui#home'
  get '/docs', to: 'ui#docs'
  get '/home', to: 'ui#home'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
